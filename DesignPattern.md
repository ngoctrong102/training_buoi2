## Design pattern Builder

***Đặt vấn đề:***
Các hàm constructor trong java được sử dụng để tạo ra các đối tượng trong chương trình. Các hàm constructor sẽ nhận các param vào để thiết lập các attributes cho đối tượng hoặc thực hiện các thao tác cần thiết. 
Vấn đề là khi số lượng các param quá nhiều có thể gây nhầm lẫn trong quá trình code. 
Trường hợp khác, trong số các param của hàm constructor có thể bao gồm các đối số bắt buộc và các đối số không bắt buộc. Trường hợp này, ta phải tạo nhiều hàm constructor, gây khó khăn trong quá trình code và maintain.
Để giải quyết vấn đề trên, ý tưởng là ta sẽ giao việc tạo đối tượng cho một đối tượng khác và đây là cách Builder làm việc.

**Định nghĩa**
Builder pattern là một trong những Creational pattern. Builder pattern là mẫu thiết kế đối tượng được tạo ra để xây dựng một đôi tượng phức tạp bằng cách sử dụng các đối tượng đơn giản và sử dụng tiếp cận từng bước, việc xây dựng các đối tượng đôc lập với các đối tượng khác.

**Cấu trúc Builder pattern**
![](image/builder.jpg)
- Product : đại diện cho đối tượng cần tạo, đối tượng này phức tạp, có nhiều thuộc tính.
- Builder : là abstract class hoặc interface khai báo phương thức tạo đối tượng.
- ConcreteBuilder : kế thừa Builder và cài đặt chi tiết cách tạo ra đối tượng. Nó sẽ xác định và nắm giữ các thể hiện mà nó tạo ra, đồng thời nó cũng cung cấp phương thức để trả các các thể hiện mà nó đã tạo ra trước đó.
- Director: là nơi sẽ gọi tới Builder để tạo ra đối tượng.

**Cài đặt Builder pattern**
*Vấn đề*: Áp dụng Builder pattern tạo các object User, trong đó mỗi User gồm  tên, tuổi, địa chỉ.

Product: class User
![](image/user.png)

Builder: interface UserBuilder
![](image/UserBuilder.png)

ConcreteBuilder: class UserConcreteBuilder implements UserBuilder
![](image/UserConcreteBuilder.png)

Director: 
![](image/main.png)