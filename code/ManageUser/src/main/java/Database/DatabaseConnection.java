package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    public static Connection connection = null;

    public static Connection getConnection() {
        if (connection== null) {
            try {
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ManageUser","root","");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return connection;
    }
}
