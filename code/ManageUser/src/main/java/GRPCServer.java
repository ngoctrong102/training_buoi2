import Services.UserService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class GRPCServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        int port = 9090;
        Server server = ServerBuilder.forPort(port).addService(new UserService()).build();
        server.start();
        System.out.println("Server listen at " + port);
        server.awaitTermination();
    }
}
