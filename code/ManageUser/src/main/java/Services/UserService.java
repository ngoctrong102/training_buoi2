package Services;

import Database.DatabaseConnection;
import com.ngoctrong.grpc.User;
import com.ngoctrong.grpc.UserServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.sql.*;
import java.util.ArrayList;


public class UserService extends UserServiceGrpc.UserServiceImplBase {
    @Override
    public void add(User.UserInfor request, StreamObserver<User.AddResponse> responseObserver) {
        String username = request.getUsername();
        int age = request.getAge();

        User.AddResponse.Builder res = User.AddResponse.newBuilder();
        Connection connection = DatabaseConnection.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO Users(username,age) values(?,?)");
            statement.setString(1, username);
            statement.setInt(2, age);
            statement.execute();

            res.setCode(0).setMessage("SUCCESS");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            res.setCode(1).setMessage("Error Database");
        } finally {
            responseObserver.onNext(res.build());
            responseObserver.onCompleted();
        }

    }

    @Override
    public void update(User.UserData request, StreamObserver<User.UpdateResponse> responseObserver) {
        String username = request.getUsername();
        int age = request.getAge();
        int id = request.getId();
        User.UpdateResponse.Builder res = User.UpdateResponse.newBuilder();
        Connection connection = DatabaseConnection.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE Users SET username=?, age=? WHERE id=?");
            statement.setString(1, username);
            statement.setInt(2, age);
            statement.setInt(3, id);
            statement.execute();

            res.setCode(0).setMessage("SUCCESS");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            res.setCode(1).setMessage("Error Database");
        } finally {
            responseObserver.onNext(res.build());
            responseObserver.onCompleted();
        }
    }

    @Override
    public void delete(User.UserId request, StreamObserver<User.DeleteResponse> responseObserver) {
        int id = request.getId();
        User.DeleteResponse.Builder res = User.DeleteResponse.newBuilder();
        Connection connection = DatabaseConnection.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Users WHERE id=?");
            statement.setInt(1, id);
            statement.execute();

            res.setCode(0).setMessage("SUCCESS");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            res.setCode(1).setMessage("Error Database");
        } finally {
            responseObserver.onNext(res.build());
            responseObserver.onCompleted();
        }
    }

    @Override
    public void get(User.UserId request, StreamObserver<User.UserData> responseObserver) {
        int id = request.getId();
        User.UserData.Builder res = User.UserData.newBuilder();
        Connection connection = DatabaseConnection.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Users WHERE id=?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                res.setUsername(resultSet.getString("username"))
                        .setAge(resultSet.getInt("age"))
                        .setId(resultSet.getInt("id"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
//            res.
        } finally {
            responseObserver.onNext(res.build());
            responseObserver.onCompleted();
        }
    }

    @Override
    public void getAll(User.Empty request, StreamObserver<User.Users> responseObserver) {
        User.Users.Builder res = User.Users.newBuilder();
        Connection connection = DatabaseConnection.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Users");
            ResultSet resultSet = statement.executeQuery();
            ArrayList<User.UserData> users = new ArrayList<>();
            while (resultSet.next()) {
                User.UserData.Builder userBuilder = User.UserData.newBuilder();
                userBuilder.setId(resultSet.getInt("id")).setUsername(resultSet.getString("username")).setAge(resultSet.getInt("age"));
                users.add(userBuilder.build());
            }
            res.addAllUsers(users);
//            res.setUsers()
        } catch (SQLException throwables) {
            throwables.printStackTrace();
//            res.
        } finally {
            responseObserver.onNext(res.build());
            responseObserver.onCompleted();
        }
    }
}
