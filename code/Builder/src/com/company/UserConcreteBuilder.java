package com.company;

public class UserConcreteBuilder implements UserBuilder{
    private String username;
    private int age;
    private String address;
    @Override
    public UserBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    @Override
    public UserBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    @Override
    public UserBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public User build() {
        return new User(username,age,address);
    }
}
