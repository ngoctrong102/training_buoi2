package com.company;

public interface UserBuilder {
    UserBuilder setUsername(String username);

    UserBuilder setAge(int age);

    UserBuilder setAddress(String address);

    User build();
}
