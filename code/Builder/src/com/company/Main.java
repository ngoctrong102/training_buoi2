package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        UserConcreteBuilder userBuilder = new UserConcreteBuilder();
        User me = userBuilder.setUsername("Võ Ngọc Trọng").setAddress("Bình Định").setAge(20).build();
        System.out.println(me.getUsername());
    }
}
